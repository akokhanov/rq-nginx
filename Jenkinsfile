pipeline {
  agent { label 'docker' }
  environment {
    appName = "rq-nginx"
  }
  parameters {
    string(name: 'KubeConfigId', defaultValue: 'jenkins-s2srqdev-kubernetes', description: 'Kubernetes cluster to deploy')
    string(name: 'ImageTag', defaultValue: '1.0', description: 'Docker image tag')
    string(name: 'KubeNamespace', defaultValue: 's2srqdev', description: 'Kubernetes namespace to deploy')
    string(name: 'KubeReplicas', defaultValue: '1', description: 'Number of kubernnetes Replicas')
    string(name: 'gceProject', defaultValue: 's2s-rq-dev', description: 'GCE project where we should deploy')
    string(name: 'gcrHost', defaultValue: 'us.gcr.io', description: 'GCR host (registry)')
  }
  stages {
    stage('Pull build rq-ui-quote') {
      when {
        expression {params.KubeConfigId != ''}
      }
      steps {
        dir('rq-ui-quote') {
          git branch: 'develop2kube',
            credentialsId: '5ffa7d14-55ec-4fd3-815c-345b5d4eac4a',
            url: 'git@bitbucket.org:star2star/rq-ui.git'
            script {
              finalImageTag = "${params.ImageTag}"
              gceProject = "${params.gceProject}"
              gcrHost = "${params.gcrHost}"
              sh '''
                rm -rf config.json
                cp config.dev2kube.json config.json
                sed -i -e '/version/ s/",/-'${finalImageTag}'",/' package.json
                ## Delete build directory
                if [ -d "./build" ]; then
                  rm -rf ./build
                fi
                ## Delete modules directory
                if [ -d "./node_modules" ]; then
                  rm -rf ./node_modules
                fi
                ## Build app
                npm install
                npm run build
                cd build
                tar cvzf ../rq-ui-quote.tgz *
                ls -la
              '''
            }
        }  
      }
    }
    stage('Pull and build rq-ui-proposal'){
      when {
        expression {params.KubeConfigId != ''}
      }
      steps {
        dir('rq-ui-proposal') {
        git branch: 'hybrid/master2kube',
          credentialsId: '5ffa7d14-55ec-4fd3-815c-345b5d4eac4a',
          url: 'git@bitbucket.org:star2star/rq-ui.git'
          script {
            sh '''
              rm -rf config.json
              cp config.dev2kube.json config.json
              sed -i -e '/version/ s/",/-'${finalImageTag}'",/' package.json
              ## Delete build directory
              if [ -d "./build" ]; then
                rm -rf ./build
              fi
              ## Delete modules directory
              if [ -d "./node_modules" ]; then
                rm -rf ./node_modules
              fi
              ## Build app
              npm install
              npm run build
              cd build
              tar cvzf ../rq-ui-proposal.tgz *
              ls -la
            '''
          }
        }
      }  
    }
    stage('Pull, build and push nginx to Gcloud Registry') {
      when {
        expression {params.KubeConfigId != ''}
      }
      steps {
        dir('rq-ui-nginx') {
          git branch: 'master',
            credentialsId: '5ffa7d14-55ec-4fd3-815c-345b5d4eac4a',
            url: 'git@bitbucket.org:akokhanov/rq-nginx.git'
          script {
            //sh "ls -la ${pwd()}"
            //sh "ls -la ../"
            echo "FINAL IMAGE TAG: ${finalImageTag}"
            sh "tar xzpf ../rq-ui-quote/rq-ui-quote.tgz --directory var/www/rq-ui/quote/"
            sh "tar xzpf ../rq-ui-proposal/rq-ui-proposal.tgz --directory var/www/rq-ui/proposal/"
            def app = docker.build("${gceProject}/${appName}:${finalImageTag}")
            docker.withRegistry("https://${gcrHost}",'gcr:jenkins-s2srqdev-grc-credentials') {
              app.push()
            }
          }
        }
      }  
    }
    stage('Deploy to kubernetes') {
      when {
        expression {params.KubeConfigId != ''}
      }
      environment {
        IMAGETAG = "${finalImageTag}"
        NAMESPACE = "${params.KubeNamespace}"
        REPLICAS = "${params.KubeReplicas}"
      }
      steps {
        kubernetesDeploy(kubeconfigId: "${params.KubeConfigId}", configs: '*.yaml', enableConfigSubstitution: true)
      }
    }
    stage('Clear unused images') {
      steps {
          echo 'IMAGES TO DELETE:'
          sh """
            docker images -f reference="${gceProject}/${appName}"
            docker images -f reference="${gcrHost}/${gceProject}/${appName }"
          """
          echo "DELETING IMAGES..."
          sh """
            docker images -q -f reference="${gceProject}/${appName}" | xargs --no-run-if-empty docker rmi -f
            docker images -q -f reference="${gcrHost}/${gceProject}/${appName}" | xargs --no-run-if-empty docker rmi -f
          """
          echo "IMAGES AFTER CLEANUP:"
          sh "docker image ls"
      }
    }
  }
  options {
    buildDiscarder(logRotator(numToKeepStr: '3'))
  }
}
