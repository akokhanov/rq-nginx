#FROM us.gcr.io/s2s-rq-dev/rq-ui-quote:latest as quote 
#WORKDIR /app/build

#FROM us.gcr.io/s2s-rq-dev/rq-ui-proposal:latest as proposal
#WORKDIR /app/build

FROM nginx:1.15

RUN apt-get -y update && \
    apt-get -y install procps wget net-tools iputils-ping bind9-host dnsutils geoip-database nano python gettext-base
RUN rm -f /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/
COPY vhost.conf /etc/nginx/conf.d/
WORKDIR /var/www/rq-ui
RUN mkdir /var/www/rq-ui/quote && mkdir /var/www/rq-ui/proposal
COPY var/www/rq-ui/quote /var/www/rq-ui/quote
COPY var/www/rq-ui/proposal /var/www/rq-ui/proposal

CMD ["nginx", "-g", "daemon off;"]